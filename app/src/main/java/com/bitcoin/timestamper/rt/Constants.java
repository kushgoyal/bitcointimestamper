package com.bitcoin.timestamper.rt;

/**
 * Created by kush on 24/10/16. BitcoinTimestamper
 */

class Constants {

    private Constants() {
        //no objects
    }

    static final String WALLET_FILES_FOLDER = "timestamper";
    static final String WALLET_FILES_PREFIX = "timestamper";
    static final String USER_AGENT = "Bitcoin-Timestamper";
    static final String APP_VERSION = "1.0";

}
