package com.bitcoin.timestamper.rt;

import android.app.Application;
import com.google.common.eventbus.EventBus;
import org.bitcoinj.utils.BriefLogFormatter;
import org.greenrobot.greendao.database.Database;

/**
 * Created by kush on 20/10/16. BitcoinTimestamper
 */

public class TimestamperApplication extends Application {

    public EventBus eventBus;
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        eventBus = new EventBus();
        // to product formatted logs
        BriefLogFormatter.init();
        DbUpdateHelper helper = new DbUpdateHelper(this, "timestamper-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}
