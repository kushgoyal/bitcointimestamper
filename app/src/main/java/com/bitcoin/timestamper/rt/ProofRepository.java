package com.bitcoin.timestamper.rt;

import android.content.Context;

import java.util.List;

/**
 * Created by kush on 01/11/16. BitcoinTimestamper
 */

public class ProofRepository {

    static Proof getProofByFilePath(Context c, String filePath) {
        List<Proof> ps = getProofDao(c).queryBuilder()
                .where(ProofDao.Properties.FilePath.eq(filePath))
                .limit(1).list();
        if (ps != null && ps.size() > 0){
            return ps.get(0);
        }
        return null;
    }

    static List<Proof> getAllProofs(Context c) {
        return getProofDao(c).loadAll();
    }

    static void insert(Context c, Proof p) {
        getProofDao(c).insert(p);
    }

    static ProofDao getProofDao(Context c) {
        return ((TimestamperApplication) c).getDaoSession().getProofDao();
    }


}
