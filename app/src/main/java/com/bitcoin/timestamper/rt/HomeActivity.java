package com.bitcoin.timestamper.rt;

import android.Manifest;
import android.content.*;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.common.eventbus.Subscribe;
import org.bitcoinj.core.*;
import org.bitcoinj.core.listeners.BlocksDownloadedEventListener;
import org.bitcoinj.core.listeners.PeerConnectedEventListener;
import org.bitcoinj.core.listeners.PreMessageReceivedEventListener;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletCoinsReceivedEventListener;
import org.bitcoinj.wallet.listeners.WalletCoinsSentEventListener;

import javax.annotation.Nullable;
import java.util.concurrent.Executor;

public class HomeActivity extends AppCompatActivity {

    private TextView mBalanceView, mAddressView, mPeersConnected;
    private ProgressBar mBlocksProgress;
    private WalletAppKit mKit;
    private WalletService mService;
    boolean mBound = false;
    private Executor runOnUiThreadExecutor;
    private TimestamperApplication mApplication;

    private static final int WRITE_STORAGE_CAMERA_PERMISSION = 1;

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to WalletService, cast the IBinder and get WalletService instance
            WalletService.WsBinder binder = (WalletService.WsBinder) service;
            mService = binder.getService();
            if (mService.getWalletKit() == null) {
                mService.setUpWalletKit(null);
            } else {
                onWalletSetupComplete(mService.getWalletKit());
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
        runOnUiThreadExecutor = new Executor() {
            @Override
            public void execute(@NonNull Runnable runnable) {
                runOnUiThread(runnable);
            }
        };
        // bind to service
        Intent intent = new Intent(this, WalletService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
        mApplication = (TimestamperApplication) getApplication();
        mApplication.eventBus.register(this);
        askForPermissions();
    }

    @Override
    protected void onDestroy() {
        mApplication.eventBus.unregister(this);
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    void initViews() {
        mBalanceView = (TextView) findViewById(R.id.value_balance);
        mAddressView = (TextView) findViewById(R.id.value_address);
        mPeersConnected = (TextView) findViewById(R.id.value_peers_connected);
        mBlocksProgress = (ProgressBar) findViewById(R.id.progress_bar_blocks_downloaded);
        Button receiveCoinsButton = (Button) findViewById(R.id.button_receive_bitcoins);
        receiveCoinsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, DisplayAddressActivity.class);
                intent.putExtra(DisplayAddressActivity.ADDRESS_KEY, mAddressView.getText().toString());
                startActivity(intent);
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Bitcoin Receiving Address", mAddressView.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(HomeActivity.this, "Your wallet address copied to clipboard", Toast.LENGTH_LONG).show();
                Log.d("bitcoin.timestamper.log", mAddressView.getText().toString());
            }
        });
        Button sendCoinsButton = (Button) findViewById(R.id.button_send_bitcoins);
        sendCoinsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SendCoinsActivity.class));
            }
        });
        Button viewTransactionsButton = (Button) findViewById(R.id.button_view_transaction_list);
        viewTransactionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, TransactionListActivity.class));
            }
        });
        Button timestampFileButton = (Button) findViewById(R.id.button_timestamp_file);
        timestampFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(HomeActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    askForPermissions();
                } else {
                    startActivity(new Intent(HomeActivity.this, TimestampFileActivity.class));
                }
            }
        });
    }

    @Subscribe
    public void blocksDownloadStatus(BlocksDownloadEvent event) {
        mBlocksProgress.setProgress((int) event.percentage);
    }

    @Subscribe
    public void onWalletSetupComplete(WalletAppKit walletAppKit) {
        mKit = walletAppKit;
        setupWalletFields();
    }

    public void setupWalletFields() {
        mAddressView.setText(mKit.wallet().currentReceiveAddress().toString());
        mBalanceView.setText(mKit.wallet().getBalance().toFriendlyString());
        mPeersConnected.setText("Peers connected " + mKit.peerGroup().numConnectedPeers());
        mKit.wallet().addCoinsReceivedEventListener(runOnUiThreadExecutor, new WalletCoinsReceivedEventListener() {
            @Override
            public void onCoinsReceived(Wallet wallet, Transaction tx, Coin prevBalance, Coin newBalance) {
                mBalanceView.setText(newBalance.toFriendlyString());
                mAddressView.setText(mKit.wallet().currentReceiveAddress().toString());
            }
        });

        mKit.wallet().addCoinsSentEventListener(runOnUiThreadExecutor, new WalletCoinsSentEventListener() {
            @Override
            public void onCoinsSent(Wallet wallet, Transaction tx, Coin prevBalance, Coin newBalance) {
                mBalanceView.setText(newBalance.toFriendlyString());
            }
        });

        mKit.peerGroup().addBlocksDownloadedEventListener(runOnUiThreadExecutor, new BlocksDownloadedEventListener() {
            @Override
            public void onBlocksDownloaded(Peer peer, Block block, @Nullable FilteredBlock filteredBlock, int blocksLeft) {
                Log.d("bitcoin.timestamper.log", "blocks left "+blocksLeft);
                if (blocksLeft == 0) {
                    mBlocksProgress.setProgress(100);
                }
            }
        });

        mKit.peerGroup().addConnectedEventListener(runOnUiThreadExecutor, new PeerConnectedEventListener() {
            @Override
            public void onPeerConnected(Peer peer, int peerCount) {
                mPeersConnected.setText("peers connected " + String.valueOf(peerCount));
            }
        });

        // check
        // https://github.com/bitcoinj/bitcoinj/blob/master/examples/src/main/java/org/bitcoinj/examples/DoubleSpend.java
        mKit.peerGroup().addPreMessageReceivedEventListener(new PreMessageReceivedEventListener() {
            @Override
            public Message onPreMessageReceived(Peer peer, Message m) {
                return null;
            }
        });

    }


    void askForPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                WRITE_STORAGE_CAMERA_PERMISSION);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_STORAGE_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    askForPermissions();
                }
            }
        }
    }
}
