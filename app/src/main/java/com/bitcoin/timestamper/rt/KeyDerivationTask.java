package com.bitcoin.timestamper.rt;

import android.os.AsyncTask;
import android.util.Log;
import com.google.protobuf.ByteString;
import org.bitcoinj.crypto.KeyCrypterScrypt;
import org.bitcoinj.wallet.Protos;
import org.spongycastle.crypto.params.KeyParameter;


/**
 * Created by kush on 22/10/16. BitcoinTimestamper
 */

class KeyDerivationTask extends AsyncTask<String, String, KeyParameter> {

    private KeyCrypterScrypt mScrypt;

    static final Protos.ScryptParameters SCRYPT_PARAMETERS = Protos.ScryptParameters.newBuilder()
            .setP(2)
            .setR(2)
            .setN(16384)
            .setSalt(ByteString.copyFrom(KeyCrypterScrypt.randomSalt()))
            .build();

    KeyDerivationTask(KeyCrypterScrypt scrypt) {
        if (scrypt == null) {
            mScrypt = new KeyCrypterScrypt(SCRYPT_PARAMETERS);
        } else {
            mScrypt = scrypt;
        }

    }

    @Override
    protected KeyParameter doInBackground(String... passwords) {
        KeyParameter result = null;
        try {
            Log.d("bitcoin.timestamper.log", passwords[0]);
            result = mScrypt.deriveKey(passwords[0]);
        } catch (Throwable e) {
            // todo handle this: show error to user
            Log.e("bitcoin.timestamper.log", e.getMessage());
        }
        return result;
    }

    KeyCrypterScrypt getScrypt() {
        return mScrypt;
    }
}
