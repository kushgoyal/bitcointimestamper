package com.bitcoin.timestamper.rt;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.bitcoinj.kits.WalletAppKit;
import org.spongycastle.crypto.params.KeyParameter;

public class PasswordNewActivity extends AppCompatActivity {

    private WalletService mService;
    private WalletAppKit mKit;
    boolean mBound = false;
    private EditText mPassword1, mPassword2;
    private ProgressDialog mDialog;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to WalletService, cast the IBinder and get WalletService instance
            WalletService.WsBinder binder = (WalletService.WsBinder) service;
            mService = binder.getService();
            mKit = mService.getWalletKit();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_new);
        initViews();
        mDialog = new ProgressDialog(this);
        mDialog.setCancelable(false);
        Intent intent = new Intent(this, WalletService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    private void initViews(){
        mPassword1 = (EditText) findViewById(R.id.input_password1);
        mPassword2 = (EditText) findViewById(R.id.input_password2);
        Button setPasswordButton = (Button) findViewById(R.id.button_set_new_password);
        setPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                encryptWallet();
            }
        });
    }

    private void encryptWallet() {
        String password1 = mPassword1.getText().toString();
        String password2 = mPassword2.getText().toString();
        if (password1.length() < 10) {
            Toast.makeText(this, "Password should be longer than 10 chars", Toast.LENGTH_LONG).show();
        } else if (!password1.contentEquals(password2)) {
            Toast.makeText(this, "Passwords don't match", Toast.LENGTH_LONG).show();
        } else {
            mDialog.setMessage("Encrypting wallet");
            mDialog.show();
            new KeyDerivationTask(null){
                @Override
                protected void onPostExecute(KeyParameter aesKey) {
                    super.onPostExecute(aesKey);
                    if (mKit != null) {
                        mKit.wallet().encrypt(getScrypt(), aesKey);
                    }
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                    startActivity(new Intent(PasswordNewActivity.this, HomeActivity.class));
                    finish();
                }
            }.execute(password1);
        }
    }

}
