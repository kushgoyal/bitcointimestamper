package com.bitcoin.timestamper.rt;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import org.bitcoinj.core.*;
import org.bitcoinj.kits.WalletAppKit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProofListActivity extends AppCompatActivity implements ProofAdapter.onVerifyTimestampListener {

    private WalletAppKit mKit;
    private WalletService mService;
    boolean mBound = false;
    private ProofAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressDialog pb;

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to WalletService, cast the IBinder and get WalletService instance
            WalletService.WsBinder binder = (WalletService.WsBinder) service;
            mService = binder.getService();
            mKit = mService.getWalletKit();
            mBound = true;
            List<Proof> proofs = ProofRepository.getAllProofs(getApplicationContext());
            mAdapter = new ProofAdapter(proofs, mService.getNetworkParams(), ProofListActivity.this);
            mRecyclerView.setAdapter(mAdapter);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proof_list);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_proof_list);
        pb = new ProgressDialog(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        // bind to service
        Intent intent = new Intent(this, WalletService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    @Override
    public void verify(Proof p) {
        pb.setMessage("Verifying proof");
        if (!pb.isShowing()) {
            pb.show();
        }
        new VerifyProofTask().execute(p);
    }

    class VerifyProofTask extends AsyncTask<Proof, Void, String> {

        @Override
        protected String doInBackground(Proof... proofs) {
            // Hash the document
            final Proof p = proofs[0];
            File doc = new File(p.getFilePath());
            Sha256Hash hash = null;
            try {
                hash = Sha256Hash.of(doc);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Verify the hash is in the OP_RETURN output of the tx
            byte[] txBytes = Utils.HEX.decode(p.getTx());
            Transaction transaction = new Transaction(mKit.params(), txBytes);
            boolean found = false;
            for (TransactionOutput output : transaction.getOutputs()) {
                // continue until output id OP_RETURN
                if (!output.getScriptPubKey().isOpReturn()) continue;
                // verify OP_RETURN value
                if (!Arrays.equals(output.getScriptPubKey().getChunks().get(1).data, hash.getBytes())) {
                    return "Hash does not match OP_RETURN output";
                }
                // hash matched
                found = true;
                break;
            }
            // no op return
            if (!found) {
                return "No OP_RETURN output in transaction";
            }
            // Verify the transaction is in the Merkle proof
            byte[] merkleBytes = Utils.HEX.decode(p.getPartialMerkleTree());
            PartialMerkleTree tree = new PartialMerkleTree(mKit.params(), merkleBytes, 0);
            List<Sha256Hash> hashes = new ArrayList<>();
            Sha256Hash merkleRoot = tree.getTxnHashAndMerkleRoot(hashes);
            Sha256Hash blockHash = Sha256Hash.wrap(p.getBlockHash());
            // if block does not contain the transaction
            if (!hashes.contains(transaction.getHash())) {
                return "Transaction not found in Merkle proof";
            }
            // Find the block given the hash
            if (mKit.peerGroup().getConnectedPeers().size() > 0) {
                Peer peer = mKit.peerGroup().getConnectedPeers().get(0);
                Block block = null;
                try {
                    block = peer.getBlock(blockHash).get();
                } catch (InterruptedException | ExecutionException e) {
                    return "Unable to fetch block please try again. Reason: " + e.getMessage();
                }
                if (block != null) {
                    if (block.getMerkleRoot().equals(merkleRoot)) {
                        return "Proof valid! Document existed at " + block.getTime();
                    } else {
                        return "Merkle root does not match block header";
                    }
                } else {
                    return "Could not find given block hash: " + blockHash.toString();
                }
            } else {
                return "Waiting for some peers to connect";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pb.dismiss();
            new AlertDialog.Builder(ProofListActivity.this)
                    .setMessage(s)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
    }

}
