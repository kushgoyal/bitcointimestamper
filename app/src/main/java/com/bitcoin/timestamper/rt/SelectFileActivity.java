package com.bitcoin.timestamper.rt;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class SelectFileActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    private FileAdapter mAdapter;
    private ListView mListView;
    private Button mBackButton;
    private File mCurrentDir, mParentDir;
    private ArrayList<File> mFiles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_file);
        mListView = (ListView) findViewById(R.id.list_files);
        mBackButton = (Button) findViewById(R.id.button_parent_dir);
        mBackButton.setOnClickListener(this);
        mAdapter = new FileAdapter(this, mFiles);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mCurrentDir = Environment.getExternalStorageDirectory();
        mParentDir = null;
        mBackButton.setText(mCurrentDir.getPath());
        getFileList(mCurrentDir);
    }

    public void getFileList(File dir) {
        if (dir != null && dir.listFiles() != null) {
            mFiles.clear();
            mFiles.addAll(Arrays.asList(dir.listFiles()));
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        final File file = mFiles.get(position);
        if (file.isDirectory()) {
            mParentDir = mCurrentDir;
            mCurrentDir = file;
            mBackButton.setText(mCurrentDir.getPath());
            getFileList(mCurrentDir);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm Selection");
            builder.setMessage("Select "+file.getName()+" ?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent();
                    intent.putExtra(TimestampFileActivity.FILE_PATH, file.getAbsolutePath());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }

    }

    @Override
    public void onClick(View view) {
        if (mParentDir == null || mCurrentDir.getParentFile() == null) {
            Toast.makeText(this, "Cant go back", Toast.LENGTH_LONG).show();
        } else {
            // set parent
            mParentDir = mCurrentDir.getParentFile();
            // set current dir
            mCurrentDir = mParentDir;
            // change file list
            getFileList(mCurrentDir);
            // change back button text
            mBackButton.setText(mCurrentDir.getPath());

        }
    }

}
