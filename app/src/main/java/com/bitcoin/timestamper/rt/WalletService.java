package com.bitcoin.timestamper.rt;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.PeerAddress;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.spongycastle.crypto.params.KeyParameter;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.concurrent.Executor;

/**
 * Created by kush on 23/10/16. BitcoinTimestamper
 */

public class WalletService extends Service {

    // Binder given to clients
    private final IBinder mBinder = new WsBinder();

    private TimestamperApplication application;

    private WalletAppKit mKit;
    private Handler mHandler;
    private NetworkParameters mParams;
    // to run tasks on the main ui thread
    private Executor runOnUiThreadExecutor;

    KeyParameter aesKey;

    private void runOnUiThread(Runnable runnable) {
        mHandler.post(runnable);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("bitcoin.timestamper.log", "Service on create called");
        mHandler = new Handler();
        // check https://bitcoinj.github.io/getting-started-java#a-note-about-writing-gui-apps
        runOnUiThreadExecutor = new Executor() {
            @Override
            public void execute(@NonNull Runnable runnable) {
                runOnUiThread(runnable);
            }
        };
        Threading.USER_THREAD = runOnUiThreadExecutor;
        application = (TimestamperApplication) getApplication();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("bitcoin.timestamper.log", "Service on bind called");
        return mBinder;
    }

    public class WsBinder extends Binder {
        WalletService getService() {
            return WalletService.this;
        }
    }

    void setUpWalletKit(@Nullable DeterministicSeed seed) {
        if (mKit != null) {
            // don't create wallet twice in one app session
            return;
        }
        if (BuildConfig.USE_MAIN_NETWORK) {
            mParams = MainNetParams.get();
        } else {
            //mParams = RegTestParams.get();
            mParams = TestNet3Params.get();
        }
        mKit = new WalletAppKit(mParams, new File(getFilesDir().getAbsolutePath()
                + File.separator + Constants.WALLET_FILES_FOLDER),
                Constants.WALLET_FILES_PREFIX + "-" + mParams.getPaymentProtocolId()) {
            @Override
            protected void onSetupCompleted() {
                super.onSetupCompleted();
                // Ensures we always have at least one key.
                if (this.wallet().getKeyChainGroupSize() < 1) {
                    wallet().importKey(new ECKey());
                }
                wallet().allowSpendingUnconfirmedTransactions();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        application.eventBus.post(mKit);
                    }
                });
            }
        };

        // define how to connect based on network params
        if (mParams == RegTestParams.get()) {
            try {
                PeerAddress pa = new PeerAddress(mParams, InetAddress.getByName("192.168.1.4"), 19000);
                mKit.setPeerNodes(pa);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        mKit.setBlockingStartup(false);
        mKit.setUserAgent(Constants.USER_AGENT, Constants.APP_VERSION);
        mKit.setDownloadListener(new DownloadProgressTracker(){
            @Override
            protected void progress(final double pct, int blocksSoFar, Date date) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        application.eventBus.post(new BlocksDownloadEvent(pct));
                    }
                });
            }
        });

        if (seed != null) {
            mKit.restoreWalletFromSeed(seed);
        }

        // check if the app is already running. Wallet kit setup throws exception if started twice
        try {
            if (mKit.isChainFileLocked()) {
                //todo check
                stopSelf();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // add listener for failure events
        mKit.addListener(new com.google.common.util.concurrent.Service.Listener() {
            @Override
            public void failed(com.google.common.util.concurrent.Service.State from, Throwable failure) {
                super.failed(from, failure);
                // todo
            }
        }, runOnUiThreadExecutor);

        // Download the block chain and wait until it's done.
        mKit.startAsync();

    }

    @Override
    public void onDestroy() {
        Log.d("bitcoin.timestamper.log", "Service on destroy called");
        // initiates service shutdown and returns immediately
        if (mKit != null) {
            mKit.stopAsync();
            // Waits for the Service to reach the terminated state.
            mKit.awaitTerminated();
        }
        super.onDestroy();
    }

    WalletAppKit getWalletKit() {
        return mKit;
    }

    NetworkParameters getNetworkParams() {
        return mParams;
    }

    KeyParameter getAesKey() {
        return aesKey;
    }

    void setAesKey(KeyParameter aesKey) {
        this.aesKey = aesKey;
    }
}
