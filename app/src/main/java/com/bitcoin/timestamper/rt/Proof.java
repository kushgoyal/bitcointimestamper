package com.bitcoin.timestamper.rt;

import org.greenrobot.greendao.annotation.*;

/**
 * Created by kush on 01/11/16. BitcoinTimestamper
 */

@Entity
public class Proof {

    @Id
    private long id;
    @NotNull
    private String filePath;
    @NotNull
    private String tx;
    @NotNull
    private String partialMerkleTree;
    @NotNull
    @Unique
    private String blockHash;
    @Generated(hash = 2036703977)
    public Proof(long id, @NotNull String filePath, @NotNull String tx,
            @NotNull String partialMerkleTree, @NotNull String blockHash) {
        this.id = id;
        this.filePath = filePath;
        this.tx = tx;
        this.partialMerkleTree = partialMerkleTree;
        this.blockHash = blockHash;
    }
    @Generated(hash = 1696071744)
    public Proof() {
    }
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getFilePath() {
        return this.filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getTx() {
        return this.tx;
    }
    public void setTx(String tx) {
        this.tx = tx;
    }
    public String getPartialMerkleTree() {
        return this.partialMerkleTree;
    }
    public void setPartialMerkleTree(String partialMerkleTree) {
        this.partialMerkleTree = partialMerkleTree;
    }
    public String getBlockHash() {
        return this.blockHash;
    }
    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

}
