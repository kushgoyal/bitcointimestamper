package com.bitcoin.timestamper.rt;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import org.bitcoinj.core.*;
import org.bitcoinj.core.listeners.BlocksDownloadedEventListener;
import org.bitcoinj.crypto.KeyCrypterScrypt;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.spongycastle.crypto.params.KeyParameter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TimestampFileActivity extends AppCompatActivity implements PasswordAskDialog.PasswordConfirmListener {

    static final int SELECT_FILE = 1;
    static final String FILE_PATH = "file_path";

    private WalletAppKit mKit;
    private NetworkParameters mParams;
    private WalletService mService;
    boolean mBound = false;
    private String mFilePath = "";
    private TextView mSelectedFileView, mTrHashView;

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to WalletService, cast the IBinder and get WalletService instance
            WalletService.WsBinder binder = (WalletService.WsBinder) service;
            mService = binder.getService();
            mKit = mService.getWalletKit();
            mParams = mService.getNetworkParams();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timestamp_file);
        initViews();
        // bind to service
        Intent intent = new Intent(this, WalletService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_FILE) {
            if (resultCode == RESULT_OK) {
                mFilePath = data.getStringExtra(FILE_PATH);
                mSelectedFileView.setText(new File(mFilePath).getName());
            }
        }
    }

    void timestampFile() throws IOException, InsufficientMoneyException, ECKey.KeyIsEncryptedException {
        File doc = new File(mFilePath);
        // Hash it
        Sha256Hash hash = Sha256Hash.of(doc);

        final Proof p = new Proof();
        p.setId(SystemClock.currentThreadTimeMillis());
        p.setFilePath(mFilePath);

        // Create a tx with an OP_RETURN output
        final Transaction tx = new Transaction(mParams);
        tx.addOutput(Coin.ZERO, ScriptBuilder.createOpReturnScript(hash.getBytes()));
        SendRequest sendRequest = SendRequest.forTx(tx);
        sendRequest.feePerKb = sendRequest.feePerKb.times(2);
        sendRequest.aesKey = mService.getAesKey();
        // Send it to the Bitcoin network
        Wallet.SendResult result = mKit.wallet().sendCoins(sendRequest);

        p.setTx(Utils.HEX.encode(tx.bitcoinSerialize()));

        mTrHashView.setText(result.tx.getHashAsString());

        // Grab the merkle branch when it appears in the block chain
        mKit.peerGroup().addBlocksDownloadedEventListener(new BlocksDownloadedEventListener() {
            @Override
            public void onBlocksDownloaded(Peer peer, Block block, FilteredBlock filteredBlock, int blocksLeft) {
                List<Sha256Hash> hashes = new ArrayList<>();
                PartialMerkleTree tree = filteredBlock.getPartialMerkleTree();
                tree.getTxnHashAndMerkleRoot(hashes);
                if (hashes.contains(tx.getHash())) {
                    p.setPartialMerkleTree(Utils.HEX.encode(tree.bitcoinSerialize()));
                    p.setBlockHash(filteredBlock.getHash().toString());
                }
            }
        });

        // Wait for confirmations (3)
        tx.getConfidence().addEventListener(new TransactionConfidence.Listener() {
            @Override
            public void onConfidenceChanged(TransactionConfidence confidence, ChangeReason reason) {
                if (confidence.getConfidenceType() != TransactionConfidence.ConfidenceType.BUILDING)
                    return;
                if (confidence.getDepthInBlocks() == 3) {
                    // Save the proof to disk
                    ProofRepository.insert(getApplicationContext(), p);
                }
            }
        });
    }

    void initViews() {
        mTrHashView = (TextView) findViewById(R.id.value_transaction_hash);
        mTrHashView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String explorerUrl = BuildConfig.EXPLORER_URL+mTrHashView.getText();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(explorerUrl));
                startActivity(browserIntent);
            }
        });
        mSelectedFileView = (TextView) findViewById(R.id.value_selected_file);
        Button selectFile = (Button) findViewById(R.id.button_select_file);
        selectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(TimestampFileActivity.this,
                        SelectFileActivity.class), SELECT_FILE);
            }
        });
        Button timeStampFileButton = (Button) findViewById(R.id.button_timestamp_file);
        timeStampFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mFilePath.isEmpty()) {
                    try {
                        timestampFile();
                    } catch (IOException e) {
                        Toast.makeText(TimestampFileActivity.this,
                                "Cannot hash file. Please select a different file", Toast.LENGTH_LONG).show();
                    } catch (InsufficientMoneyException e) {
                        Toast.makeText(TimestampFileActivity.this,
                                "Not enough coins in your wallet. " + e.missing.getValue() +
                                        " satoshis are missing including fees", Toast.LENGTH_LONG).show();
                    } catch (ECKey.KeyIsEncryptedException e) {
                        Toast.makeText(TimestampFileActivity.this,
                                "Please enter password and try  again", Toast.LENGTH_LONG).show();
                        new PasswordAskDialog().show(getSupportFragmentManager(), "askPassword");
                    }
                } else {
                    Toast.makeText(TimestampFileActivity.this,
                            "Please select a file first", Toast.LENGTH_LONG).show();
                }
            }
        });
        findViewById(R.id.button_view_proofs_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TimestampFileActivity.this, ProofListActivity.class));
            }
        });
    }

    @Override
    public void onPasswordConfirmed(String password) {
        final ProgressDialog pb = new ProgressDialog(this);
        pb.setMessage("checking password");
        pb.show();
        new KeyDerivationTask((KeyCrypterScrypt) mKit.wallet().getKeyCrypter()){
            @Override
            protected void onPostExecute(KeyParameter keyParameter) {
                super.onPostExecute(keyParameter);
                pb.dismiss();
                if (keyParameter != null && mKit.wallet().checkAESKey(keyParameter)){
                    mService.setAesKey(keyParameter);
                } else {
                    Toast.makeText(TimestampFileActivity.this, "enter password again", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(password);
    }
}
