package com.bitcoin.timestamper.rt;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kush on 01/11/16. BitcoinTimestamper
 */

class ProofAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Proof> proofs = new ArrayList<>();
    private NetworkParameters mParams;
    private onVerifyTimestampListener mVerifyListener;


    ProofAdapter(List<Proof> proofs, NetworkParameters params, onVerifyTimestampListener vl) {
        this.proofs.addAll(proofs);
        this.mParams = params;
        this.mVerifyListener = vl;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_proof, parent, false);
        viewHolder = new ProofViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Proof p = proofs.get(position);
        ((ProofViewHolder) holder).bind(p);
    }

    @Override
    public int getItemCount() {
        return proofs.size();
    }

    private class ProofViewHolder extends RecyclerView.ViewHolder {

        private TextView mFilePathView, mTxHashView;
        private Button mVerifyButton;

        ProofViewHolder(View itemView) {
            super(itemView);
            mFilePathView = (TextView) itemView.findViewById(R.id.value_proof_filepath);
            mTxHashView = (TextView) itemView.findViewById(R.id.value_proof_transaction_hash);
            mVerifyButton = (Button) itemView.findViewById(R.id.button_proof_verify);
        }

        void bind(final Proof proof) {
            mFilePathView.setText(proof.getFilePath());
            byte[] txBytes = Utils.HEX.decode(proof.getTx());
            Transaction transaction = new Transaction(mParams, txBytes);
            mTxHashView.setText(transaction.getHashAsString());
            mVerifyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mVerifyListener.verify(proof);
                }
            });
        }

    }

    interface onVerifyTimestampListener {
        void verify(Proof p);
    }

}
