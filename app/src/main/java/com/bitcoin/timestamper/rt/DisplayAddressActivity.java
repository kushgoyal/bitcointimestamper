package com.bitcoin.timestamper.rt;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class DisplayAddressActivity extends AppCompatActivity {

    ImageView mQrCodeView;
    String address;
    Bitmap mBitmap;
    public final static int WIDTH = 500, HEIGHT=500;

    static final String ADDRESS_KEY = "addressKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_address);
        address = getIntent().getStringExtra(ADDRESS_KEY);
        mQrCodeView = (ImageView) findViewById(R.id.image_address_qr_code);
        final ProgressDialog pb = new ProgressDialog(this);
        pb.setMessage("Generating Qr Code");
        pb.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mBitmap = encodeAsBitmap(address);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mQrCodeView.setImageBitmap(mBitmap);
                            pb.dismiss();
                        }
                    });
                } catch (WriterException e) {
                    Toast.makeText(DisplayAddressActivity.this, "Cannot create qr code", Toast.LENGTH_LONG).show();
                }
            }
        }).start();

    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ?
                        ContextCompat.getColor(this, android.R.color.black) :
                        ContextCompat.getColor(this, android.R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    }
}
