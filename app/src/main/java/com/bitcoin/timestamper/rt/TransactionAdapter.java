package com.bitcoin.timestamper.rt;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.wallet.Wallet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kush on 25/10/16. BitcoinTimestamper
 */

public class TransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Wallet wallet;
    private ArrayList<Transaction> transactions = new ArrayList<>();
    private Map<Sha256Hash, TransactionCacheEntry> transactionCache = new HashMap<>();
    private Activity activity;

    public TransactionAdapter(ArrayList<Transaction> transactions, Wallet wallet, Activity activity) {
        this.wallet = wallet;
        this.transactions = transactions;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_transaction, parent, false);
        viewHolder = new TransactionViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Transaction item = transactions.get(position);
        ((TransactionViewHolder) holder).bind(item);
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    private class TransactionViewHolder extends RecyclerView.ViewHolder {

        TextView txHashView, toAddressView, amountView, isSentView;

        TransactionViewHolder(View itemView) {
            super(itemView);
            txHashView = (TextView) itemView.findViewById(R.id.value_transaction_id);
            toAddressView = (TextView) itemView.findViewById(R.id.value_to_address);
            amountView = (TextView) itemView.findViewById(R.id.value_transaction_amount);
            isSentView = (TextView) itemView.findViewById(R.id.value_is_sent_transaction);
        }

        void bind(Transaction tx) {
            TransactionCacheEntry txCache = transactionCache.get(tx.getHash());
            if (txCache == null) {
                final Coin value = tx.getValue(wallet);
                final boolean sent = value.signum() < 0;
                final Coin fee = tx.getFee();
                final Address address;
                if (sent)
                    address = WalletUtils.getToAddressOfSent(tx, wallet);
                else
                    address = WalletUtils.getWalletAddressOfReceived(tx, wallet);
                txCache = new TransactionCacheEntry(value, sent, fee, address);
                transactionCache.put(tx.getHash(), txCache);
            }
            txHashView.setText(tx.getHashAsString());
            amountView.setText(txCache.value.toFriendlyString());
            toAddressView.setText(txCache.address != null ? txCache.address.toString() : "null");
            isSentView.setText(txCache.isSent ? "Sent":"Received");
            txHashView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String explorerUrl = BuildConfig.EXPLORER_URL+txHashView.getText();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(explorerUrl));
                    activity.startActivity(browserIntent);
                }
            });
        }

    }

    private static class TransactionCacheEntry {
        private final Coin value;
        private final boolean isSent;
        private final Coin fee;
        @Nullable private final Address address;

        private TransactionCacheEntry(final Coin value, final boolean isSent, final Coin fee,
                                      final @Nullable Address address)
        {
            this.value = value;
            this.isSent = isSent;
            this.fee = fee;
            this.address = address;
        }
    }


}
