package com.bitcoin.timestamper.rt;

import android.content.Context;
import org.greenrobot.greendao.database.Database;

/**
 * Created by kush on 01/11/16. BitcoinTimestamper
 */

class DbUpdateHelper extends DaoMaster.OpenHelper {

    DbUpdateHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        // todo for future versions. for schema 1 do nothing
        ProofDao.dropTable(db, true);
        onCreate(db);
    }
}
