package com.bitcoin.timestamper.rt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


class FileAdapter extends BaseAdapter {

    private ArrayList<File> filesList = new ArrayList<>();
    private LayoutInflater mInflater;


    FileAdapter(Context context, ArrayList<File> filesList) {
        this.filesList = filesList;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return filesList.size();
    }

    @Override
    public File getItem(int position) {
        return filesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_list_import_local_files, parent , false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        File file = filesList.get(position);
        holder.bind(file);

        return convertView;
    }

    private static class ViewHolder {

        ImageView fileTypeImage;
        TextView fileName;

        ViewHolder(View view) {
            this.fileTypeImage = (ImageView) view.findViewById(R.id.image_file_type);
            this.fileName = (TextView) view.findViewById(R.id.value_file_name);
        }

        void bind(File file) {
            if (file.isDirectory()) {
                fileTypeImage.setImageResource(R.drawable.ic_folder_grey600_36dp);
            } else {
                fileTypeImage.setImageResource(R.drawable.ic_description_grey600_36dp);
            }
            fileName.setText(file.getName());
        }
    }

}
