package com.bitcoin.timestamper.rt;

/**
 * Created by kush on 25/10/16. BitcoinTimestamper
 */

public class BlocksDownloadEvent {

    double percentage;

    public BlocksDownloadEvent(double percentage) {
        this.percentage = percentage;
    }

}
