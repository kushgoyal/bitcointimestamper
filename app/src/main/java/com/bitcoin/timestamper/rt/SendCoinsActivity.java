package com.bitcoin.timestamper.rt;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import org.bitcoinj.core.*;
import org.bitcoinj.crypto.KeyCrypterScrypt;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletCoinsSentEventListener;
import org.spongycastle.crypto.params.KeyParameter;

import java.util.concurrent.Executor;

public class SendCoinsActivity extends AppCompatActivity implements View.OnClickListener,
        PasswordAskDialog.PasswordConfirmListener {

    private WalletAppKit mKit;
    private WalletService mService;
    boolean mBound = false;
    private TextView mBalanceView, mLastTransactionIdView;
    private EditText mAddressInput, mValueInput;
    private Executor runOnUiThreadExecutor;
    private ProgressBar mProgressBar;

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to WalletService, cast the IBinder and get WalletService instance
            WalletService.WsBinder binder = (WalletService.WsBinder) service;
            mService = binder.getService();
            mKit = mService.getWalletKit();
            mBound = true;
            mBalanceView.setText(mKit.wallet().getBalance().toFriendlyString());
            mKit.wallet().addCoinsSentEventListener(runOnUiThreadExecutor, new WalletCoinsSentEventListener() {
                @Override
                public void onCoinsSent(Wallet wallet, Transaction tx, Coin prevBalance, Coin newBalance) {
                    mBalanceView.setText(newBalance.toFriendlyString());
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_coins);
        initViews();
        runOnUiThreadExecutor = new Executor() {
            @Override
            public void execute(@NonNull Runnable runnable) {
                runOnUiThread(runnable);
            }
        };
        // bind to service
        Intent intent = new Intent(this, WalletService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                mAddressInput.setText(result.getContents());
            }
        }
    }

    void initViews() {
        mBalanceView = (TextView) findViewById(R.id.value_current_balance);
        mAddressInput = (EditText) findViewById(R.id.input_send_to_address);
        mValueInput = (EditText) findViewById(R.id.input_send_value);
        mLastTransactionIdView = (TextView) findViewById(R.id.value_last_transaction_id);
        Button confirmButton = (Button) findViewById(R.id.button_send_confirm);
        confirmButton.setOnClickListener(this);
        findViewById(R.id.button_scan_address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new IntentIntegrator(SendCoinsActivity.this).initiateScan();
            }
        });
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar_transaction_depth);
    }

    @Override
    public void onClick(View view) {
        String valueString = mValueInput.getText().toString();
        Coin value = Coin.ZERO;
        try {
            value = Coin.parseCoin(valueString);
        } catch (Exception ignored) {
            //ignore
        }
        String addressString = mAddressInput.getText().toString();
        Address to = null;
        try {
            to = Address.fromBase58(mKit.wallet().getNetworkParameters(), addressString);
        } catch (AddressFormatException e) {
            Toast.makeText(this, "address invalid", Toast.LENGTH_LONG).show();
        }
        if (value != Coin.ZERO && to != null) {
            sendCoins(to, value);
        }
    }

    void sendCoins(Address to, Coin value) {
        // There are different ways to create and publish a SendRequest. This is probably the easiest one.
        // Have a look at the code of the SendRequest class to see what's
        // happening and what other options you have:
        // https://bitcoinj.github.io/javadoc/0.11/com/google/bitcoin/core/Wallet.SendRequest.html
        //
        // Please note that this might raise a InsufficientMoneyException if your wallet has not enough
        // coins to spend.
        // When using the testnet you can use a faucet (like the http://faucet.xeno-genesis.com/)
        // to get testnet coins.
        // In this example we catch the InsufficientMoneyException and register a
        // BalanceFuture callback that runs once the wallet has enough balance.
        try {
            SendRequest req = SendRequest.to(to, value);
            // should be already set
            req.aesKey = mService.getAesKey();
            Wallet.SendResult result = mKit.wallet().sendCoins(mKit.peerGroup(), req);
            mLastTransactionIdView.setText(result.tx.getHashAsString());
            mProgressBar.setProgress(0);
            result.tx.getConfidence().addEventListener(runOnUiThreadExecutor, new TransactionConfidence.Listener() {
                @Override
                public void onConfidenceChanged(TransactionConfidence confidence, ChangeReason reason) {
                    if (confidence.getConfidenceType() != TransactionConfidence.ConfidenceType.BUILDING)
                        return;
                    if (confidence.getDepthInBlocks() < 6) {
                        mProgressBar.setProgress(confidence.getDepthInBlocks() * 20);
                    }
                }
            });
        } catch (InsufficientMoneyException e) {
            Toast.makeText(this, "Not enough coins in your wallet. Missing " +
                    e.missing.getValue() + " satoshis are missing (including fees)", Toast.LENGTH_LONG).show();
        } catch (ECKey.KeyIsEncryptedException e) {
            Toast.makeText(this, "Please enter password and try  again", Toast.LENGTH_LONG).show();
            new PasswordAskDialog().show(getSupportFragmentManager(), "askPassword");
        } catch (AddressFormatException e) {
            // already handled
        }
    }


    @Override
    public void onPasswordConfirmed(String password) {
        final ProgressDialog pb = new ProgressDialog(this);
        pb.setMessage("checking password");
        pb.show();
        new KeyDerivationTask((KeyCrypterScrypt) mKit.wallet().getKeyCrypter()){
            @Override
            protected void onPostExecute(KeyParameter keyParameter) {
                super.onPostExecute(keyParameter);
                pb.dismiss();
                if (keyParameter != null && mKit.wallet().checkAESKey(keyParameter)){
                    mService.setAesKey(keyParameter);
                } else {
                    Toast.makeText(SendCoinsActivity.this, "enter password again", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(password);
    }
}
