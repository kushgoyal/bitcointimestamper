package com.bitcoin.timestamper.rt;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.common.eventbus.Subscribe;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.UnreadableWalletException;

import java.io.File;

public class SetupWalletActivity extends AppCompatActivity implements View.OnClickListener {

    private WalletService mService;
    boolean mBound = false, mWalletExists = false;
    private EditText mSeedCodeInput;
    private EditText mCreationTimeInput;
    private ProgressDialog mDialog;
    private TimestamperApplication mApplication;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to WalletService, cast the IBinder and get WalletService instance
            WalletService.WsBinder binder = (WalletService.WsBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_wallet);
        if (new File(getFilesDir().getAbsolutePath()
                + File.separator + Constants.WALLET_FILES_FOLDER).exists()) {
            // it means a wallet already exists
            mWalletExists = true;
        }
        initViews();
        mApplication = (TimestamperApplication) getApplication();
        mDialog = new ProgressDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mApplication.eventBus.register(this);
        Intent intent = new Intent(this, WalletService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        mApplication.eventBus.unregister(this);
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onStop();
    }

    private void initViews() {
        Button restoreWalletButton = (Button) findViewById(R.id.button_restore_from_seed);
        Button createWalletButton = (Button) findViewById(R.id.button_create_new_wallet);
        if (mWalletExists) {
            createWalletButton.setText("Open Existing Wallet");
        }
        mSeedCodeInput = (EditText) findViewById(R.id.text_input_mnemonic_seed);
        mCreationTimeInput = (EditText) findViewById(R.id.text_input_creation_time);
        restoreWalletButton.setOnClickListener(this);
        createWalletButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_restore_from_seed:
                String seedCode = mSeedCodeInput.getText().toString();
                String creationTimeString = mCreationTimeInput.getText().toString();
                Long creationTime = 0L;
                try {
                    creationTime = Long.valueOf(creationTimeString.isEmpty() ? "0" : creationTimeString);
                } catch (NumberFormatException e) {
                    // pass
                }
                if (!seedCode.isEmpty() && creationTime != 0L) {
                    try {
                        DeterministicSeed seed = new DeterministicSeed(seedCode, null, "", creationTime);
                        createWallet(seed);
                    } catch (UnreadableWalletException e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, "enter correct seed and time", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.button_create_new_wallet:
                createWallet(null);
                break;
            default:
                return;
        }
    }

    private void createWallet(@Nullable DeterministicSeed seed) {
        if (mService != null) {
            mDialog.setMessage("Setting up wallet");
            mDialog.show();
            mService.setUpWalletKit(seed);
        }
    }

    @Subscribe
    public void onWalletSetupComplete(WalletAppKit walletAppKit) {
        mDialog.dismiss();
        if (!mWalletExists) {
            final DeterministicSeed seed = walletAppKit.wallet().getKeyChainSeed();
            AlertDialog.Builder builderForCode = new AlertDialog.Builder(this);
            builderForCode = builderForCode.setTitle("Mnemonic Code and creation time").setCancelable(false);
            builderForCode.setPositiveButton("done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    startActivity(new Intent(SetupWalletActivity.this, PasswordNewActivity.class));
                    finish();
                }
            }).setMessage(
                    "Mnemonic Code:\n"+seed.getMnemonicCode()+"\n"+
                            "Creation time:\n"+String.valueOf(seed.getCreationTimeSeconds())
            ).show();
        } else {
            if (walletAppKit.wallet().isEncrypted()) {
                Log.d("bitcoin.timestamper.log", "Wallet setup complete");
                startActivity(new Intent(this, HomeActivity.class));
            } else {
                startActivity(new Intent(this, PasswordNewActivity.class));
            }
            finish();
        }
    }

}
